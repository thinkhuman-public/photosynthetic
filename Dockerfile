FROM python:3.8.6-slim-buster

WORKDIR /usr/src/app

RUN apt-get update && apt-get install -y automake gcc g++ subversion python3-dev

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

RUN mkdir /palette_images/

CMD [ "python", "-u", "./main.py" ]