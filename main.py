import json
import sys
from src.photosynthetic.Palette import Palette

if __name__ == '__main__':

    filepath = sys.argv[1]
    p = Palette(filepath)
    palette = p.extract()

    print(json.dumps(palette), end='')


    # file_path = os.path.join('resources', 'images', 'test2.jpeg')
    #
    # colors_path = join('resources/colors.json')
    # with open(colors_path, 'r') as f:
    #     colors = json.loads(f.read())
    #
    #
    # counter = 1
    #
    # segments_count = 370
    # rag = 5
    # target_palette_size=4
    #
    # palette = Palette(file_path)
    # palette.configure(segments_count=segments_count, adjacent_region_distance_threshold=rag, counter=counter,
    #                   target_palette_size=target_palette_size)
    #
    # palette.base_colors = colors['data']
    #
    # image_analysis = palette.extract()
    # quit()
    #
    #
    # for i in range(0, 100):
    #     print(counter)
    #
    #     segments_count += 10
    #
    #     if i % 10 == 0:
    #        rag += 1
    #
    #     if i % 25 == 0:
    #        target_palette_size += 1
    #
    #     # Analysis Profile Parameters
    #
    #     palette = Palette(file_path)
    #     palette.configure(segments_count=segments_count, adjacent_region_distance_threshold=rag, counter=counter, target_palette_size=target_palette_size)
    #
    #     palette.base_colors = colors['data']
    #
    #
    #     image_analysis = palette.extract()
    #     counter += 1
