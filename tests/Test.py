import json

import requests

from src import Palette


class Tests:
    def __init__(self):

        self.jackets = [
            # 'tree4.jpg',
            # 'green-2.jpg',
            # 'jacket2.jpg',
            'LrktXrNo7Kgr9gaWasBCRa2jePYzQtwX5CWh1jQi.png',
            # 'redjacket.jpg',
            # 'yellowandblackjacket.jpg',
            # 'darkredjacket.jpg',
            # 'jacket3.jpg',
        ]

        pass

    def run(self, segments=500, test='jackets'):

        colors = []

        colors_req = requests.get('https://local.photosynthernet.dev/api/colors')

        if colors_req.status_code == 200:
            colors = json.loads(colors_req.content)

        images_to_test = getattr(self, '%s' % test)
        for i in images_to_test:
            palette = Palette('source_images/' + i)

            palette.base_colors = colors['resources']

            palette.segments = segments
            palette.file_name = i
            pal = palette.extract()
            print(pal)
